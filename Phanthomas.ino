#include <ClickEncoder.h>
#include <TimerOne.h>
#include <EEPROM.h>
#include "U8glib.h"

#define ADC_FILTER_LENGTH 64

// State machine
#define INHALE		0
#define GOTO_EXHALE	1
#define EXHALE		2
#define GOTO_INHALE	3
#define MEAS		4

#define DEAD_TIME_MS	100
#define SAVE_TIME	5000 // 5 s

#define GRAPH_LENGTH    96

U8GLIB_DOGS102 u8g(10, 8);

ClickEncoder *encoder;
ClickEncoder::Button b;

#define TRIG_IN 2
#define VALVE   3
#define LED     9
#define SENSOR  A2

          unsigned int uiLastPeriod = 0;
          unsigned int uiLowerVal = 200u;
          unsigned int uiUpperVal = 230u;
volatile  unsigned int uiCurVal   = 0u;
volatile  unsigned int uiDeadTime = 0u;
volatile  unsigned int uiSaveCnt  = SAVE_TIME;
volatile  unsigned int uiPeriod   = 6000;

volatile unsigned char aubGraph[GRAPH_LENGTH];
volatile unsigned char ubGraphPos = 0;

volatile unsigned char ubTimeRes = 100;

unsigned char ubState = INHALE;
boolean  bChangeLower = true;

void timerIsr() {
  static unsigned char ubGraphTimer = 0;
    
  ubGraphTimer++;
  if (ubGraphTimer >= ubTimeRes) {
    ubGraphTimer = 0;
    aubGraph[ubGraphPos] = 64 - (uiCurVal/ADC_FILTER_LENGTH - uiLowerVal + 10) / 2;
    ubGraphPos++;
    if (ubGraphPos >= GRAPH_LENGTH ) ubGraphPos = 0;
  }
  
  uiDeadTime++;
  if (uiPeriod >= 0) uiPeriod++;
  if (uiSaveCnt) uiSaveCnt--;
  encoder->service();
}



ISR(ADC_vect) {
  static          boolean  bInit = true;
  static unsigned int     auiFilterBuffer[ADC_FILTER_LENGTH];
  static unsigned char    ubBufferPos = 0;
	
  if (bInit) {
    for (unsigned char iI = 0; iI < ADC_FILTER_LENGTH; iI ++) auiFilterBuffer[iI] = 0;
    bInit = false;
  }		
	
  unsigned int uiCurrentVal = ADC & 0x03FF;
	
  uiCurVal -= auiFilterBuffer[ubBufferPos];
  uiCurVal += uiCurrentVal;
  auiFilterBuffer[ubBufferPos] = uiCurrentVal;
		
  ubBufferPos++;
  if (ubBufferPos >= ADC_FILTER_LENGTH) ubBufferPos = 0;
}



void setup() {
  encoder = new ClickEncoder(5, 6, 4, 4);
  encoder->setAccelerationEnabled(1);
  Timer1.initialize(1000);
  Timer1.attachInterrupt(timerIsr);
  
  pinMode(SENSOR, INPUT);
  pinMode(VALVE , OUTPUT);
  pinMode(LED   , OUTPUT);
  digitalWrite(LED, 1);
  
  /* Init ADC: Free running mode und interrupt freischalten, Filterung im Interrupt */
  ADMUX = (1 << REFS1) | 0x02;				/* Input 2, internal 2.56 V reference */
  ADCSRA = (1 << ADEN) | (1 << ADSC) | (1 << ADATE) | (1 << ADIE) | (7 << ADPS0);   /* Enable, prescaler 128 (damit Interruptlast nicht zu hoch) */
}

void draw(void) {
  
  u8g.setColorIndex(1);
  
  u8g.setFont(u8g_font_micro);
  u8g.drawStr(0, 6, "Lower Value");
  u8g.setFont(u8g_font_helvB12);
  u8g.drawHLine(0, 8, 45);
  u8g.setPrintPos(0, 22);
  u8g.print(uiLowerVal);
  
  u8g.setFont(u8g_font_micro);
  u8g.drawStr(56, 6, "Upper Value");
  u8g.setFont(u8g_font_helvB12);
  u8g.drawHLine(56, 8, 45);
  u8g.setPrintPos(56, 22);
  u8g.print(uiUpperVal);

  u8g.setFont(u8g_font_micro);
  u8g.drawStr(0, 32, "Current Val");
  u8g.setFont(u8g_font_helvB12);
  u8g.drawHLine(0, 34, 45);
  u8g.setPrintPos(0, 48);
  u8g.print(uiCurVal / ADC_FILTER_LENGTH);
  
  u8g.setFont(u8g_font_micro);
  u8g.drawStr(56, 32, "Period [ms]");
  u8g.setFont(u8g_font_helvB12);
  u8g.drawHLine(56, 34, 45);
  u8g.setPrintPos(56, 48);
  u8g.print(uiLastPeriod);
}


void plot(void) {
  u8g.setColorIndex(1);
  u8g.setFont(u8g_font_micro);
  
  for (unsigned char iI = 1; iI < GRAPH_LENGTH; iI++) {
    if (iI - 1 != ubGraphPos) u8g.drawLine(iI - 1, aubGraph[iI - 1], iI, aubGraph[iI]);
  }
  
  
  u8g.setPrintPos(0, 6);
  u8g.print(uiLowerVal);
  
  u8g.setPrintPos(45, 6);
  u8g.print(uiCurVal/ADC_FILTER_LENGTH);
  
  u8g.setPrintPos(90, 6);
  u8g.print(uiUpperVal);
}

void loop() {
  
  u8g.firstPage();
  do {
    plot();
  } while( u8g.nextPage() );
  
  if (bChangeLower) {
    uiLowerVal += encoder->getValue();
  } else {
    uiUpperVal += encoder->getValue();
  }

  switch(ubState) {
    case INHALE:
      digitalWrite(VALVE, 1);
      if(uiCurVal / ADC_FILTER_LENGTH >= uiUpperVal) {
	uiDeadTime = 0;			
	ubState = GOTO_EXHALE;
      }
      break;
			
    case GOTO_EXHALE:
      digitalWrite(VALVE, 0);
      if(uiDeadTime >= DEAD_TIME_MS) ubState = EXHALE;
      break;
			
    case EXHALE:
      digitalWrite(VALVE, 0);
      if(uiCurVal / ADC_FILTER_LENGTH <= uiLowerVal) ubState = MEAS;
      break;
			
    case MEAS:
      uiLastPeriod = uiPeriod;
      uiPeriod = 0;
      uiDeadTime = 0;
      ubState = GOTO_INHALE;
      break;
			
    case GOTO_INHALE:
      digitalWrite(VALVE, 1);
      if(uiDeadTime >= DEAD_TIME_MS) ubState = INHALE;
      break;
  }		
		
		/*if ((!un16SaveCnt) && (un8Changed)) {
			eeprom_write_word(EE_EXHALED, aun16Val[0]);
			eeprom_write_word(EE_INHALED, aun16Val[1]);
		}*/			
		
  
 
  b = encoder->getButton();
  switch (b) {
      case ClickEncoder::Open: break;
      case ClickEncoder::Pressed: break;
      case ClickEncoder::Held: break;
      case ClickEncoder::Released:
        if (bChangeLower) bChangeLower = false; else bChangeLower = true;
        break;
      case ClickEncoder::Clicked: break;
      case ClickEncoder::DoubleClicked: break;
   }
   
   delay(10);
}

