# Phanthomas #

Welcome to the Phanthomas resource page. Phanthomas is an MR-compatible phantom for the simulation of non-rigid motion using compressed air. It basically consists of three Components:

* Pneumatics
* Control Electronics
* The Actual Phantom

This page provides detailed building instructions for the first two. The design of the actual phantom is greatly influenced by your needs and will be given few consideration here. We want to note, however, that we found test lungs such as [this one](http://www.mspinc.com/siemens-adult-1liter-test-lung-190-6006832) quite helpful as an inflatable component and much superior to simple balloons.

## Pneumatics ##

The following figure shows the basic components of the pneumatics.

![Phantom_neu.gif](https://bitbucket.org/repo/qAn4xG/images/4284637256-Phantom_neu.gif)

The needle valve at the top can be used to pre-fill the phantom and is optional. The current version of the control electronics also fetaures a pre-fill function via the solenoud (electrically controlled) valve. The baffle is also optional - usually your MRI will be much more noisy than the pneumatics.

Several issues ocurred to us while testing the phantom. First, you need quite a bit of air, so your main tubing should have a sufficient diameter (outer) of about 10 mm. Particularly the tube from the pneumatics to the phantom causes quite some flow resistance, so bigger is better. The second point concerns the point of measuring the phantom's pressure, i.e. the point to which the control electronic's pressure sensor is connected to. Though this could be done right at the solenoid valve, we recommend using a second, thinner tube and connect it as close to the phantom as possible. This way, a more stable behaviour of the control circuit can be achived and accidential oscillations due to pressure waves at the valve are omitted.

### Part List ###

* 1 3/2-way solenoid valve, 1/4" fittings, preferably 12 V (then you can run the electronics of a single 12 V power supply)
* 2 flow limiters, 1/4"
* thick tubing (~10 mm outer diameter)
* thin tubing (6 mm outer diameter) to connect the phatom to the PCB's pressure sensor
* multiple screw fittings (depends on your components). Make sure they match the component's threads and the tubing diameter.
* 1 T-shaped connector right at the phantom. Has two ports fitting your thick tubing and one port fitting the thin tubing.
* a connector for your institution's compressed air supply

Optional:

* needle valve (for pre-filling)
* 2 T-shaped connectors fitting your thick tubing (only if needle valve installed)
* baffle (if you like it quiet)

### Assembly ###

Use a base plate of your choosing (preferrably not ferromagnetic :)) to mount the components. Cut the tubing to the desired lengths and connect according to the schematic above.

## Electronics ##

The control electronics is based on an extension board (shield) to an Arduino Uno open hardware environment. The Arduino can be bought pre-assembled, the shield has to be assembled by yourself.

### Part List ###

The required parts of the shield can be found in a bill of materials in the [repository](https://bitbucket.org/Christian_Wuerslin/phanthomas/downloads). I have created a ready-to-go [Inventory](https://secure.reichelt.de/index.html?&ACTION=20&AWKID=924890&PROVID=2084) of the German online shop "Reichelt" which ships to all of Europe and beyond (however not the US) so you can save some work. US folks are referred to [Mouser](http://www.mouser.com) which sells the probably most critical part, the display.

### PCB ###

We had a limited quantity of PCBs manufactured. Write an e-mail to christian.wuerslin@med.uni-tuebingen.de if you are intrested in getting one. If you have the facilities, you can also manufacture your own PCBs using the Eagle files (*.sch *.brd) in the [repository](https://bitbucket.org/Christian_Wuerslin/phanthomas/downloads) or you can have them manufactured. For small PCBs and non-profit organizations, [Eagle](http://www.cadsoft.de/download-eagle/) is a free PCB layout software.

### Assembly ###

Start assembling the shield beginning with the smallest parts (resistors, capacitors, semiconductors) first, then go on with the bigger ones. All parts except the Arduino headers (JP2 through JP5) are mounted on the top side. The following figure shows the values of the parts to be mounted

![assembly.gif](https://bitbucket.org/repo/qAn4xG/images/609267787-assembly.gif)

![IMG_20140617_092841929.jpg](https://bitbucket.org/repo/qAn4xG/images/564945090-IMG_20140617_092841929.jpg)

The small components are mounted.

![IMG_20140617_115907875.jpg](https://bitbucket.org/repo/qAn4xG/images/493590678-IMG_20140617_115907875.jpg)

Now the big ones too.

![IMG_20140617_115933672.jpg](https://bitbucket.org/repo/qAn4xG/images/3082869794-IMG_20140617_115933672.jpg)

View of the bottom side: The headers used to connect to the Arduino board.

![IMG_20140617_120111377.jpg](https://bitbucket.org/repo/qAn4xG/images/1104435255-IMG_20140617_120111377.jpg)

The completely assembled shield (except pressure sensor, sorry) mounted onto the Arduino. Two more notes:

* The pressure sensor needs to be screwed to the board for stability. Unfortunately, space under the board

## Firmware (Arduino) ##

The firmware is written in the Arduino language (basically C++). The Arduino platform has the advantage, that no additional programming hardware is needed - the code is simply uploaded via USB. To upload the firmware to the board or to implement your own features, you need the Arduino SDK.

### Get Arduino SDK ###

Go to the Arduino [Getting Started](http://arduino.cc/en/Guide/HomePage) page and follow the instructions for your OS and the Arduino Uno platform. 

### Get Firmware ###

The firmware is what this repository is all about. Get the latest version of the repository [here](https://bitbucket.org/Christian_Wuerslin/phanthomas/downloads) or clone the repository if you use git.

### Install Firmware ###

Open the phanthomas.ino in the Arduino SDK and press the upload button in the toolbar (second from left).